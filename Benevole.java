/**
 * Classe utilis�e dans une application de gestions d'employ�s.
 * 
 * En plus d'�tre un employ� r�gulier, un manager est en charge d'un d�partement
 * et re�oit un bonus � chaque semaine.
 * 
 * @author 
 * @date 
 */
public class Benevole extends Employe {
  
 // ================ Attributs ===============
 private double heuresAccumulees;

 // =========== Constructeur =================
 public Benevole(int unNumero, String unNom) {
  super(unNumero, unNom, 0.0);
  this.heuresAccumulees = 0.0;
 }

 // ======== Accesseurs/Mutateurs =================
 // get : retourne une copies des attributs
 public double getHeuresAccumulees() {
  return this.heuresAccumulees;
 }
  
 // ========== Autres m�thodes ===============
 // Calcule et retourne le salaire de la semaine, et s'occupe de l'accumulation
 // dans les bons atrtibuts
 public double encaisserSalaire(double nombreHeures) {
  this.heuresAccumulees+= nombreHeures;
  return 0.0;
 }

 // R�initialise les variables d'accumulation
 public void reinitialiserPeriodeDeCumul() {
  this.heuresAccumulees = 0.0;
 }

 public String toString() {
  return "Merci "+super.nom+" pour avoir effectu� "+String.format("%.1f",this.heuresAccumulees) +" heures de b�n�volat.";
 }

 // ========== M�thodes red�finies ===========
}

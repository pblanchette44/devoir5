/**
 * Fichier            : Devoir5.java
 * Description        : Familiarisation avec les fichiers textes avec la gestion d'employ�s.
 * Nom                : Gabry�le De Ladurantaye
 * Collaboration avec : Maude Gervais-Cloutier
 * Session            : H2020
 */
import java.util.*;
import java.io.*;
public class Devoir5 {
  
  
  public static void main(String[] args) throws Exception { 
    // D�claration des variables
    ArrayList<Employe> liste= new ArrayList<Employe>();
    Employe emp=null;
    String nomFichier="",uneLigne="";
    String[] tabInfos=null;
    int numeroDeFichier=0,nombreEmployes=0,nombreSemaines=0;
    double nombreHeures=0.0,sommeRevenus=0.0;

    // Lecture du num�ro de fichier et formation du nom de fichier 
    // ***** N'ajoutez pas de validation, je vais corriger avec mes fichiers qui ont d'autres num�ros
    //       mais le m�me format
    // numeroDeFichier=ES.lireEntier("Entrez le num�ro de fichier (1 � 4) : "); // avec la partie Z
    numeroDeFichier=ES.lireEntier("Entrez le num�ro de fichier (1 ou 2) : "); // sans la partie Z
    nomFichier="exemple"+numeroDeFichier+".txt";
    
    // A) Ouvrez le flux de donn�es 
     BufferedReader entree = new BufferedReader(new FileReader(nomFichier));
     
    // B) Effectuez la gestion des 4 premi�res lignes (incluant l'extraction du nombre d'employ�s)

     for (int i=0; i<2;i++) {
         entree.readLine();  
     }//fin for sauter 2 premi�res lignes
     
     //Lire la 3e ligne
     uneLigne = entree.readLine();
     
     //S�parer les informations de la ligne
     tabInfos=uneLigne.split(":");
     
     //enlever les espaces de chaques d�but de case.
     for (int i=0;i<tabInfos.length;i++) {
    	 tabInfos[i]=tabInfos[i].trim();
     }//Fin for trim.
     
     //Ins�rer le int (4) de la position 1 du tableau dans nombreEmployes.
     nombreEmployes=Integer.parseInt(tabInfos [1]);
     
     //sauter la 4e ligne
     entree.readLine();
     
    // C) Effectuez la boucle de gestion des employ�s
     for (int j=0; j<=nombreEmployes;j++) {
     while(uneLigne!=null) {
    	 
      // C.1) Lire les infos de l'employ�, cr�ez la bonne instance dans la variable emp.
    	 uneLigne=entree.readLine();
    	 uneLigne=uneLigne.replace(",", ":");
    	 ES.afficher("affichage ligne 62"+uneLigne);
    	 uneLigne=uneLigne.replace("#","");
    	 uneLigne=uneLigne.replace("$", "");
    	 uneLigne=uneLigne.replace("/hr", "");
    	 
    	 //S�parer la chaine en tableau
    	 tabInfos=uneLigne.split(":");
    	 
    	 //Enlever les espaces de bout de ligne
    	 for (int i=0; i<tabInfos.length;i++) {
    		 tabInfos[i]=tabInfos[i].trim();  	 
    	 }//fin for enlever les espaces de bouts de ligne
    	 
    	 //Convertir les donn�es
    	 if (tabInfos[0].equals("Employe") ) {
    		 //System.out.println("print [0] == employe:   "+tabInfos[0]);
    		 emp = new Employe(Integer.parseInt(tabInfos[1]), tabInfos[2], Double.parseDouble(tabInfos[3]));
    		 
    	 }else if (tabInfos[0].equals("Manager")){
    		 //System.out.println("print [0] == manager:   "+tabInfos[0]);
    		 emp = new Manager(Integer.parseInt(tabInfos[1]), tabInfos[2], Double.parseDouble(tabInfos[3]), tabInfos[4], Double.parseDouble(tabInfos[5]));
    		 
    	 }else if (tabInfos[0].equals("Benevole")){
    		 //System.out.println("print [0] == benevole:   "+tabInfos[0]);
    		 //for(int i =0;i<tabInfos.length;i++){
    			 //System.out.println(tabInfos[i]);
    			 //}
    		 emp = new Benevole(Integer.parseInt(tabInfos[1]), tabInfos[2]);
    	 }//fin if - convertir les donn�es en leur bon type d'employ�s
    	 
      // C.2) Passez les 2 lignes inutiles
     
    	 for (int i=0; i<2;i++) {
    		 entree.readLine();  
    	 }//fin for, passer les 2 autres lignes inutiles
     
      // C.3) Ajoutez l'employ� � la liste 
     liste.add(emp);
     }//fin while - boucle de gestion des employ�s
     }//fin for nombre employes
     for (int i=0;i<liste.size();i++) {
    	 System.out.println(liste.get(i));
     }
     
    // D) Fermer le flux de donn�es
    entree.close();
    // E) Dans une nouvelle boucle, pour chaque employ� :
    //    ... encaissez 2 semaines de travail de 40.0 heures chacune
    //        (si vous faites la partie Z, effacez ces 2 lignes)
    
    
    //    ... affichez chaque employe 
    //    ... comptez la somme de tous revenus accumul�s pare tous les travailleurs 
    //    Affichez cette somme apr�s la boucle
    
  }//Fin main();
 
}//Fin class Devoir5

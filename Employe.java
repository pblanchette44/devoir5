/**
 * Classe utilis�e dans une application de gestions d'employ�s.
 * 
 * Cette classe permet de calculer le salaire pour une semaine pour un 
 * employ�, ansi que d'accumuler ses revenus sur une longue p�riode
 * (typiquement une ann�e)
 * 
 * @author Pierre Coutu
 * @date 2019-02-10
 */
public class Employe {
  
  // ================ Attributs ===============
  protected int     numero;
  protected String  nom;
  protected double  tauxHoraire;
  protected double  salaireAccumule=0.0; // valeur par d�faut

  // =========== Constructeur =================
  /** Ce constructeur initialise 3 de ses attributs avec 
   * les 3 param�tres qu'il re�oit. Le salaireAccumule
   * conserve sa valeur par d�faut, soit 0.0
   *
   * @param unNumero (int) num�ro d'identification de l'employ�
   * @param unNom (String) nom de l'employ�
   * @param unTauxHoraire (double) montant vers� pour chaque heure travaill�e 
   */
  public Employe(int unNumero, String unNom, double unTauxHoraire) {
    numero=unNumero;
    nom=unNom;
    tauxHoraire=unTauxHoraire;
  }

  // ======== Accesseurs/Mutateurs =================
  // retourne une copies des attributs
  public int getNumero(){return numero;}
  public String getNom(){return nom;}
  public double getTauxHoraire(){return tauxHoraire;}
  // permet de modifier les attributs
  public void setTauxHoraire(double nouveauTaux){tauxHoraire=nouveauTaux;}
  
  // ========== Autres m�thodes ===============
  /** Calcule et retourne le salaire d'un employ� selon le nombre d'heures re�u
   * en param�tre. De plus, le salaire de la semaine est ajout� � l'attribut
   * salaireAccumule. Le calcul du salaire inclus la prime de surtemps pour les heures
   * qui d�passe 40 (50% de plus).
   * 
   * @param  nombreHeures (double) nombre d'heures travaill�es dans la semaine
   * @return (double) le montant d'argent gagn� par le travailler pour sa semaine de travail
   */
  public double encaisserSalaire(double nombreHeures){
    double salaireSemaine=0.0;
    if (nombreHeures < 40.0){
      salaireSemaine=nombreHeures*tauxHoraire;
    } else {
      salaireSemaine=(40.0+1.5*(nombreHeures-40.0))*tauxHoraire;
    }
    salaireAccumule+=salaireSemaine;
    return salaireSemaine;
  }
  
  /** Calcule et retourne les revenus accumul�s par l'employ�.
   * Pour l'instant ses seuls revenus sont son salaire. 
   * Plus d'option � venir vers la fin de la session ...
   * 
   * @return (double) revenus accumul�s par l'employ� (Ici, m�me chose que salaire accumul�)
   */
  public double calculerRevenuAccumule(){
    return salaireAccumule;
  }
  
  /** R�initialise tous les attributs qui accumulent des quantit�s (ici seulement salaireAccumules)*/
  public void reinitialiserPeriodeDeCumul(){
    salaireAccumule=0.0;
  }

  // ========== M�thodes red�finies ===========
  /** Formate la repr�sentation en chaine de caract�re
   * @return format  : NOM [#NUMERO] @TAUX_HORAIRE $/heure (revenu accumul� : REVENU_ACCUMULE $)
   */
  public String toString(){
    String message;
    // Version orginale :
    // message=nom+" [#"+numero+"] : @"+tauxHoraire+" $/heure (revenu accumul� : "+this.calculerRevenuAccumule()+" $)";
    // Version modifi�e (pour arrondir � 2 chirres apr�s le point) :
    message=nom+" [#"+numero+"] : @"+tauxHoraire+" $/heure (revenu accumul� : "+
            String.format("%.2f",this.calculerRevenuAccumule())+" $)";
    return message;
  }
}

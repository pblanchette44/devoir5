/**
 * Classe utilis�e dans une application de gestions d'employ�s.
 * 
 * En plus d'�tre un employ� r�gulier, un manager est en charge d'un d�partement
 * et re�oit un bonus � chaque semaine.
 * 
 * @author
 * @date
 */
public class Manager extends Employe {

 // ================ Attributs ===============
 private String departement;
 private double bonusSemaine, bonusAccumules;

 // =========== Constructeur =================
 public Manager(int unNumero, String unNom, double unTaux, String unDepartement, double unBonusSemaine) {
  super(unNumero, unNom, unTaux);
  departement = unDepartement;
  bonusSemaine = unBonusSemaine;
  bonusAccumules = 0.0;
 }

 // ======== Accesseurs/Mutateurs =================
 // get : retourne une copies des attributs
 public String getDepartement() {
  return departement;
 }

 public double getBonusSemaine() {
  return bonusSemaine;
 }

 // set : permet de modifier les attributs

 // ========== Autres m�thodes ===============

 // ========== M�thodes red�finies ===========
 // Calcule et retourne le salaire de la semaine, et s'occupe de l'accumulation
 // dans les bons atrtibuts
 public double encaisserSalaire(double nombreHeures) {
  // variables locales
  double salaire = 0.0;
  // Le parent s'occupe de la partie du salaire dues aux heures travaill�es
  salaire = super.encaisserSalaire(nombreHeures);
  // traiter les bonus
  this.bonusAccumules += this.bonusSemaine;
  salaire += this.bonusSemaine;
  // retourner le salaire de la semaine, avec les bonus
  return salaire;
 }

 // Calcule et retourne les revenus accumul�s (salaire+commissions)
 public double calculerRevenuAccumule() {
  // parties du parent (salaireAccumule dues aux heures) + commissions Accumles
  return super.calculerRevenuAccumule() + this.bonusAccumules;
 }

 // R�initialise les variables d'accumulation
 public void reinitialiserPeriodeDeCumul() {
  super.reinitialiserPeriodeDeCumul();
  this.bonusAccumules = 0.0;
 }

 public String toString() {
  return super.toString() + " (incluant les bonus)";
 }
}
